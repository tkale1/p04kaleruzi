//
//  main.m
//  asteroidsproject
//
//  Created by Jacob Ruzi on 2/20/16.
//  Copyright © 2016 Jacob Ruzi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
