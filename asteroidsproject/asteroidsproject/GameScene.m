//
//  GameScene.m
//  asteroidsproject
//
//  Created by Jacob Ruzi on 2/20/16.
//  Assistance from SpriteKit tutorial on raywenderlich.com
//  Copyright (c) 2016 Jacob Ruzi. All rights reserved.
//

#import "GameScene.h"
#import "GameOverScene.h"

static const uint32_t bulletCategory     =  0x1 << 0;
static const uint32_t astoCategory        =  0x1 << 1;
static const uint32_t shipCategory = 0x1 << 2;

@interface GameScene () <SKPhysicsContactDelegate>
@property (nonatomic) SKSpriteNode * myShip, * leftButton, * rightButton, * forwardButton, * shootButton, *asto;
@property (nonatomic) NSTimeInterval lastSpawnTimeInterval;
@property (nonatomic) NSTimeInterval lastUpdateTimeInterval;
@end

@implementation GameScene
int Score=0;
NSString * message;
SKLabelNode *Score_count;
-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        
        NSLog(@"Size: %@", NSStringFromCGSize(size));
        
        SKSpriteNode *sn = [SKSpriteNode spriteNodeWithImageNamed:@"wallpaper.jpg"];
        sn.size=CGSizeMake(self.frame.size.width, self.frame.size.height);
        sn.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        sn.name = @"BACKGROUND";
        [self addChild:sn];
    

        //self.backgroundColor = [SKColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
        
        self.myShip = [SKSpriteNode spriteNodeWithImageNamed:@"ship_new"];
        self.myShip.size=CGSizeMake(50, 50);
        self.myShip.position = CGPointMake(self.frame.size.width/2, 32);
        self.myShip.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:self.myShip.size.width/2];
        self.myShip.physicsBody.dynamic = YES;
        self.myShip.physicsBody.categoryBitMask = shipCategory;
        self.myShip.physicsBody.contactTestBitMask = astoCategory;
        self.myShip.physicsBody.collisionBitMask = 0;
        self.myShip.physicsBody.usesPreciseCollisionDetection = YES;
        [self addChild:self.myShip];
        
        //Add buttons
        self.leftButton =[SKSpriteNode spriteNodeWithImageNamed:@"left_arrow.png"];
        self.leftButton.position = CGPointMake(self.leftButton.size.width/2 + 10, self.leftButton.size.height/2 + 5);
        [self addChild: self.leftButton];
        
        self.rightButton =[SKSpriteNode spriteNodeWithImageNamed:@"right_arrow.png"];
        //self.rightButton.size=CGSizeMake(25, 25);
        self.rightButton.position = CGPointMake(self.rightButton.size.width/2 + self.leftButton.size.width + 20, self.rightButton.size.height/2 + 5);
        [self addChild: self.rightButton];
        
        self.forwardButton = [SKSpriteNode spriteNodeWithImageNamed:@"up_arrow.png"];
        self.forwardButton.position = CGPointMake(self.frame.size.width - (self.forwardButton.size.width - 10), self.forwardButton.size.height/2 + 10);
        [self addChild: self.forwardButton];
        
        self.shootButton = [SKSpriteNode spriteNodeWithImageNamed:@"Fire.png"];
        self.shootButton.size=CGSizeMake(self.forwardButton.size.width-5, self.forwardButton.size.height-5);
        self.shootButton.position = CGPointMake(self.frame.size.width - (self.forwardButton.size.width) - (self.forwardButton.size.width), self.shootButton.size.height/2 + 10);
        [self addChild: self.shootButton];
        
        self.physicsWorld.gravity = CGVectorMake(0,0);
        self.physicsWorld.contactDelegate = self;
        
        SKLabelNode *Score_label = [SKLabelNode labelNodeWithFontNamed:@"HoeflerText-Italic"];
        Score_label.text = @"SCORE  :  ";
        Score_label.fontSize = 20;
        Score_label.fontColor = [SKColor whiteColor];
        Score_label.position = CGPointMake(self.size.width-100, self.size.height-30);
        [self addChild:Score_label];

        Score_count = [SKLabelNode labelNodeWithFontNamed:@"HoeflerText-Italic"];
        Score_count.text = @"0";
        Score_count.fontSize = 28;
        Score_count.fontColor = [SKColor whiteColor];
        Score_count.position = CGPointMake(self.size.width-40, self.size.height-30);
        [self addChild:Score_count];

    }
    return self;
}

/*-(void)didMoveToView:(SKView *)view {*/
/* Setup your scene here */
/*    SKLabelNode *myLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"]; */

//   myLabel.text = @"Hello, World!";
//    myLabel.fontSize = 45;
//    myLabel.position = CGPointMake(CGRectGetMidX(self.frame),
//                                   CGRectGetMidY(self.frame));

//    [self addChild:myLabel];
//}

- (void)addasto {
    
    // Create sprite
    SKSpriteNode * asto = [SKSpriteNode spriteNodeWithImageNamed:@"asteroid"];
    asto.size=CGSizeMake(40, 40);
    asto.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:asto.size.width/2];
    asto.physicsBody.dynamic = YES;
    asto.physicsBody.categoryBitMask = astoCategory;
    asto.physicsBody.contactTestBitMask = bulletCategory;
    asto.physicsBody.collisionBitMask = 0;
    // Determine where to spawn the asto along the Y axis
    int minX = asto.size.width / 2;
    int maxX = self.frame.size.width - asto.size.width / 2;
    int rangeX = maxX - minX;
    int actualX = (arc4random() % rangeX) + minX;
    
    // Create the asto slightly off-screen along the right edge,
    // and along a random position along the Y axis as calculated above
    asto.position = CGPointMake(actualX,self.frame.size.height + asto.size.height/2);
    [self addChild:asto];
    
    // Determine speed of the asto
    int minDuration = 3.5;
    int maxDuration = 6.0;
    int rangeDuration = maxDuration - minDuration;
    int actualDuration = (arc4random() % rangeDuration) + minDuration;
    
    // Create the actions
    int X_cordinate=arc4random_uniform(8);
    SKAction * actionMove = [SKAction moveTo:CGPointMake((self.frame.size.width/X_cordinate), asto.size.height/2) duration:actualDuration];
    SKAction * actionMoveDone = [SKAction removeFromParent];
    //    SKAction * loseAction = [SKAction runBlock:^{
    //        SKTransition *reveal = [SKTransition flipHorizontalWithDuration:0.5];
    //        SKScene * gameOverScene = [[GameOverScene alloc] initWithSize:self.size won:NO];
    //        [self.view presentScene:gameOverScene transition: reveal];
    //    }];
    [asto runAction:[SKAction sequence:@[actionMove,actionMoveDone]]];
    
}

- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)timeSinceLast {
    
    self.lastSpawnTimeInterval += timeSinceLast;
    if (self.lastSpawnTimeInterval > 1) {
        self.lastSpawnTimeInterval = 0;
        [self addasto];
    }
}

- (void)update:(NSTimeInterval)currentTime {
    // Handle time delta.
    // If we drop below 60fps, we still want everything to move the same distance.
    CFTimeInterval timeSinceLast = currentTime - self.lastUpdateTimeInterval;
    self.lastUpdateTimeInterval = currentTime;
    if (timeSinceLast > 1) { // more than a second since last update
        timeSinceLast = 1.0 / 60.0;
        self.lastUpdateTimeInterval = currentTime;
    }
    
    //Allows screen to be wrapped (moves ship to opposite end when it moves off screen)
    if (self.myShip.position.x < 0)
        self.myShip.position = CGPointMake(self.frame.size.width-2, self.myShip.position.y);
    else if (self.myShip.position.x > self.frame.size.width)
        self.myShip.position = CGPointMake(2, self.myShip.position.y);
    else if (self.myShip.position.y < 0)
        self.myShip.position = CGPointMake(self.myShip.position.x, self.frame.size.height-2);
    else if (self.myShip.position.y > self.frame.size.height)
        self.myShip.position = CGPointMake(self.myShip.position.x, 2);
    
    //This does the same as above, but for bullets
    
    if ([self childNodeWithName:@"myBullet"].position.x < 0)
        [self childNodeWithName:@"myBullet"].position = CGPointMake(self.frame.size.width-2, [self childNodeWithName:@"myBullet"].position.y);
    else if ([self childNodeWithName:@"myBullet"].position.x > self.frame.size.width)
        [self childNodeWithName:@"myBullet"].position = CGPointMake(2, [self childNodeWithName:@"myBullet"].position.y);
    else if ([self childNodeWithName:@"myBullet"].position.y < 0)
        [self childNodeWithName:@"myBullet"].position = CGPointMake([self childNodeWithName:@"myBullet"].position.x, self.frame.size.height-2);
    else if ([self childNodeWithName:@"myBullet"].position.y > self.frame.size.height)
        [self childNodeWithName:@"myBullet"].position = CGPointMake([self childNodeWithName:@"myBullet"].position.x, 2);
    
    [self updateWithTimeSinceLastUpdate:timeSinceLast];
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    for (UITouch *touch in touches) {
        //CGPoint location = [touch locationInNode:self];
        
        //NSLog(@"I pressed: %@", NSStringFromCGPoint(location));
        
        SKAction *rotateLeft = [SKAction rotateByAngle:M_1_PI/2 duration:.1];
        SKAction *repeatLeft = [SKAction repeatActionForever: rotateLeft];
        SKAction *rotateRight = [SKAction rotateByAngle:-M_1_PI/2 duration:.1];
        SKAction *repeatRight = [SKAction repeatActionForever: rotateRight];
        
        CGFloat angle = self.myShip.zRotation + M_PI/2;
        SKAction *moveForward = [SKAction moveByX:10*cosf(angle) y:10*sinf(angle) duration:0.1];
        SKAction *repeatForward = [SKAction repeatActionForever: moveForward];
        
        //From raywenderlich.com/forums
        UITouch *touch = [touches anyObject];
        CGPoint positionInScene = [touch locationInNode:self];
        SKSpriteNode *touchedNode = (SKSpriteNode *)[self nodeAtPoint:positionInScene];
        
        //Check which button was pushed
        if (touchedNode == self.leftButton) {
            [self.myShip runAction: repeatLeft withKey:@"rotatingLeft"];
        } else if (touchedNode == self.rightButton) {
            [self.myShip runAction: repeatRight withKey:@"rotatingRight"];
        } else if (touchedNode == self.forwardButton) {
            [self.myShip runAction: repeatForward withKey:@"movingForward"];
        } else if (touchedNode == self.shootButton) {
            [self runAction:[SKAction playSoundFileNamed:@"Sounds/laser.wav" waitForCompletion:NO]];
            [self shootBullet];
        }
        
    }
}

- (void)shootBullet {
    SKSpriteNode * bullet = [SKSpriteNode spriteNodeWithImageNamed:@"ball1"];
    bullet.size=CGSizeMake(30, 30);
    bullet.name = @"myBullet";
    bullet.position = self.myShip.position;
    bullet.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:bullet.size.width/2];
    bullet.physicsBody.dynamic = YES;
    bullet.physicsBody.categoryBitMask = bulletCategory;
    bullet.physicsBody.contactTestBitMask = astoCategory;
    bullet.physicsBody.collisionBitMask = 0;
    bullet.physicsBody.usesPreciseCollisionDetection = YES;
    
    [self addChild: bullet];
    
    CGFloat angle = self.myShip.zRotation + M_PI/2;
    
    SKAction * actionMove = [SKAction moveByX:500*cosf(angle) y:500*sinf(angle) duration:1.2];
    SKAction * actionMoveDone = [SKAction removeFromParent];
    [bullet runAction:[SKAction sequence:@[actionMove, actionMoveDone]]];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.myShip removeActionForKey:@"rotatingLeft"];
    [self.myShip removeActionForKey:@"rotatingRight"];
    [self.myShip removeActionForKey:@"movingForward"];
}

//Adapted from tutorial referenced at beginning of file
- (void)didBeginContact:(SKPhysicsContact *)contact
{
    // 1
    SKPhysicsBody *firstBody, *secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    // 2
    if ((firstBody.categoryBitMask == bulletCategory) &&
        (secondBody.categoryBitMask == astoCategory))
    {
        [self projectile:(SKSpriteNode *) firstBody.node shotAsteroid:(SKSpriteNode *) secondBody.node];
    }
    
    else if ((firstBody.categoryBitMask == astoCategory) &&
             (secondBody.categoryBitMask == shipCategory))
    {
        [self asteroid:(SKSpriteNode *) firstBody.node hitShip:(SKSpriteNode *) secondBody.node];
    }
}

//Adapted from tutorial referenced at beginning of file
//This occurs when the player shoots an asteroid
- (void)projectile:(SKSpriteNode *)projectile shotAsteroid:(SKSpriteNode *)asteroid {
    NSLog(@"Shot Asteroid");
    [self runAction:[SKAction playSoundFileNamed:@"Sounds/explosion.wav" waitForCompletion:NO]];
    Score+=1;
    Score_count.text = [NSString stringWithFormat:@"%d",Score];
    
    [projectile removeFromParent];
    [asteroid removeFromParent];
}

//Adapted from tutorial referenced at beginning of file
//This occurs when the player is hit by an asteroid
- (void)asteroid:(SKSpriteNode *)asteroid hitShip:(SKSpriteNode *)ship {
    NSLog(@"Hit Ship");
    SKTransition *reveal = [SKTransition flipHorizontalWithDuration:0.5];
    SKScene * gameOverScene = [[GameOverScene alloc] initWithSize:self.size];
    [self.view presentScene:gameOverScene transition: reveal];
}

@end
