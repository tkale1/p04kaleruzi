//
//  AppDelegate.h
//  asteroidsproject
//
//  Created by Jacob Ruzi on 2/20/16.
//  Copyright © 2016 Jacob Ruzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

