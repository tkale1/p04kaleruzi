//
//  GameOverScene.m
//  Adaped from SpriteKitSimpleGame Tutorial
//  Created by Main Account on 9/4/13.
//
//  Jacob Ruzi, Tanmay Kale
//

#import "GameOverScene.h"
#import "GameScene.h"
 
@implementation GameOverScene
 
-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        
        [self runAction:[SKAction playSoundFileNamed:@"Sounds/explosion2.wav" waitForCompletion:NO]];
        
        SKSpriteNode *sn = [SKSpriteNode spriteNodeWithImageNamed:@"wallpaper.jpg"];
        sn.size=CGSizeMake(self.frame.size.width, self.frame.size.height);
        sn.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        sn.name = @"BACKGROUND";
        [self addChild:sn];
        self.backgroundColor = [SKColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
 
        NSString * message;
        message = @"Game Over!";
 
        SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"Zapfino"];
        label.text = message;
        label.fontSize = 40;
        label.fontColor = [SKColor whiteColor];
        label.position = CGPointMake(self.size.width/2, self.size.height/2);
        [self addChild:label];
        
        
        NSString * retrymessage;
        retrymessage = @"Play Again..!! :)";
        SKLabelNode *retryButton = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
        retryButton.text = retrymessage;
        retryButton.fontColor = [SKColor whiteColor];
        retryButton.position = CGPointMake(self.size.width/2, 50);
        retryButton.name = @"retry";
        [self addChild:retryButton];
        
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"retry"]) {
        
        SKTransition *reveal = [SKTransition flipHorizontalWithDuration:0.5];
        GameScene * scene = [GameScene sceneWithSize:self.view.bounds.size];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        [self.view presentScene:scene transition: reveal];
        
    }
}
@end
